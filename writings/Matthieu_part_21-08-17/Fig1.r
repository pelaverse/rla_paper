library(RLA)
library(tidyverse)
### series of catches
set.seed(123)
system.time(
  hp <- pellatomlinson_pbr(burnin = 1.5e2,
                           depletion0 = 0.05,
                           Rmax = 0.04,
                           catches = floor(runif(50, 1e3, 5e3))
                           )
  )
g <- summary_plot(hp, lower_zero = TRUE)
ggsave(plot = g,
       filename = "figs/PBR.jpeg", 
       dpi = 600, units = 'cm',
       width = 20, height = 15
       )

pbr_simul <- forward_pbr(pbrlist = hp,
                         distribution = "truncnorm",
                         frequency = 6,
                         horizon = 100,
                         q = 0.2,
                         F_r = 0.5
                         )

