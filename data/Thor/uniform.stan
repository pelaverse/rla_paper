data {
	int<lower = 1> n_survey; // nb of surveys
	int<lower = 1> n_year; // nb of years with removals
  vector<lower = 0.0>[n_survey] SURVEY_mean; // abundance estimates
	vector<lower = 0.0>[n_survey] SURVEY_cv;   // cv of abundance estimates
	vector<lower = 0.0>[n_year] BYCATCH; // bycatch estimates
	real<lower = 0.0> IPL; // Internal Protection Level, same scale as depletion;
  // The downweighting is implemented to reduce variability in bycatch limits (Cooke 1999)
	real<lower = 0.0, upper = 1.0> W; // weight for down-weighting the likelihood as in the IWC CLA
	real<lower = 0.0> UPPER; // upper limit for growth rate
  int<lower = 1, upper = n_year + 1> SCANS[n_survey]; // pointers to years of SCANS survey
}

transformed data {
	vector[n_survey] scale = sqrt(log1p(square(SURVEY_cv)));
	vector[n_survey] location = log(SURVEY_mean) - 0.5 * log1p(square(SURVEY_cv));
}

parameters {
	real<lower = 1E-4, upper = UPPER> r; // population growth rate
	real<lower = 1E-4, upper = 1.0> depletion; // depletion at the end of the time-series
}

transformed parameters {
	vector[n_year + 1] abundance;
	real K;
	// what's observed in the latest survey is a fraction of carrying capacity
	K = SURVEY_mean[n_survey] / depletion;
	// the population begins at carrying capacity
	abundance[1] = K;
	for(t in 1:n_year) {
		abundance[t + 1] = abundance[t] - BYCATCH[t] + r * abundance[t] * (1 - square(abundance[t] / K));
	}
}

model {
	for(i in 1:n_survey) {
		target += lognormal_lpdf(abundance[SCANS[i]]| location[i], scale[i]) * W;
	}
}

generated quantities {
	real removal_limit;
	removal_limit = r * fmax(0.0, depletion - IPL);
}
