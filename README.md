
# RLA paper <img src="data/logo_RLA/RLA_logo_2.png" align="right" width="120"/>

<br>

# Presentation

This repository stores all scripts and results of the RLA paper which is
being published in Frontiers in Marine science. It records all the work
made but also can be used as a reproducible repository for whom want to
redo, improve or test analyses.

![](data/readme_files/paper_screenshot.png)

## Prerequisites

It is necessary to install {RLA} package, to do so :

``` r
install.packages("remotes")
remotes::install_gitlab(
  host = "https://gitlab.univ-lr.fr",
  repo = "pelaverse/rla"
)
library(RLA)
```

# Organisation of the repository

2 main part can be considered :

-   PBR analysis : The scripts to perform the PBR analyses are stored at
    : [script/PBR](script/PBR)

-   RLA analysis :

    -   All output of RLA scenarios performed on supercomputer are
        stored as **RData** files at : [res/RLA](res/RLA)
    -   These output can be analysed using script
        [R/Thor/03_AnalyseOutput.r](R/Thor/03_AnalyseOutput.r)

# Case studies application

The two case studies cited in the paper are presented shiny application.
Results of each scenarios tested are presented with graphical
illustration.

-   For the PBR shiny application on small delphinide :
    <https://gitlab.univ-lr.fr/pelaverse/pbrfrtuning>

-   For the RLA shiny application on Harbour porpoise :
    <https://gitlab.univ-lr.fr/pelaverse/rlascenarioviz>
