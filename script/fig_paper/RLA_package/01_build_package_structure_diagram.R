require(DiagrammeR)
require(DiagrammeRsvg)
require(xml2)
library(magick)

path_save <- "res/fig_paper/RLA_package"
if(!(dir.exists(path_save))) {
  dir.create(path_save, recursive = T)
}


grViz("script/fig_paper/RLA_package/RLA_package_structure_V2.gv") %>% 
  export_svg() %>%
  read_xml() %>%
  write_xml(file.path(path_save,"package_structure_V2.svg"))


# logo rla part
RLA <- image_read("data/logo_RLA/RLA_logo_2.png") %>% 
  image_scale("100x100")


# save all thing
diagram <- magick::image_read_svg(file.path(path_save,"package_structure_V2.svg")) %>% 
  image_trim()

tot <- c(diagram) %>% 
  image_composite(RLA, offset = "+600+0")

tot %>% 
  image_write(file.path(path_save,"package_structure_V2.jpeg"), quality = 100)
