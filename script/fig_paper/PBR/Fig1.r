library(RLA)
library(tidyverse)


path_save <- "res/fig_paper/PBR/Fig1/"
if(!(dir.exists(path_save))) {
  dir.create(path_save, recursive = T)
}

### series of catches
set.seed(123)
system.time(
  hp <- pellatomlinson_pbr(burnin = 1.5e2,
                           depletion0 = 0.05,
                           Rmax = 0.04,
                           catches = floor(runif(50, 1e3, 5e3))
                           )
  )
g <- summary_plot(hp, lower_zero = TRUE)
ggsave(plot = g,
       filename = paste0(path_save,"/PBR.jpeg"), 
       dpi = 600, units = 'cm',
       width = 15*(3/2), height = 10*(3/2)
       )

pbr_simul <- forward_pbr(pbrlist = hp,
                         distribution = "truncnorm",
                         frequency = 6,
                         horizon = 100,
                         q = 0.2,
                         F_r = 0.5
                         )

