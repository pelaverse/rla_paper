library(RLA)
library(tidyverse)

path_save <- "res/fig_paper/RLA/Fig2"
if(!(dir.exists(path_save))) {
  dir.create(path_save, recursive = T)
}

set.seed(123)
hp <- pellatomlinson_rla(burnin = 150,
                         depletion0 = 0.1,
                         MNPL = 0.6,
                         MNP = 1.04,
                         L = 22,
                         eta = c(2, 2, rep(1, 21)),
                         phi = c(0.85, 0.87, rep(0.91, 20), 0.0),
                         m = c(0, 1 / (1 + exp( (4 - 0:21) / 0.5))),
                         catches = floor(runif(50, 1000, 5000)),
                         everything = TRUE,
                         scans = seq(10, 50, 10)
                         )

g <- summary_plot(hp, lower_zero = TRUE) +
  plot_annotation(theme = theme(plot.subtitle = element_text(size=8)))

ggsave(plot = g,
       filename = paste0(path_save,"/RLA.jpeg"), 
       dpi = 600, units = 'cm',
       width = 15*(3/2), height = 10*(3/2)
       )

data(rlastan_models)
# use uniform priors
cat(rlastan_models$uniform)
# compile model
rlastan <- rstan::stan_model(model_code = rlastan_models$uniform,
                             model_name = "Removal Limit Algorithm"
                             )

rla_simul <- forward_rla(rlalist = hp,
                         rlastan = rlastan,
                         q = 0.5,
                         distribution = "truncnorm",
                         # upper bound for the prior on r, the population growth rate
                         upper = 0.1
                         )

