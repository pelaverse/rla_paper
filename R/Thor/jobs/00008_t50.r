##--------------------------------------------------------------------------------------------------------
## SCRIPT : quantile 80%, K constant, no catastrophe, no bias
##
## Authors : Matthieu Authier
## Last update : 2021-05-03
## R version 4.0.4 (2021-02-15) -- "Lost Library Book"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

q <- 0.8
Kfinal <- 1
cata <- 0
bias_abund <- 1
bias_byc <- 1

source("tuningThor_t50.r")