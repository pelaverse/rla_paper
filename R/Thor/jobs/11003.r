##--------------------------------------------------------------------------------------------------------
## SCRIPT : quantile 30%, K constant, no catastrophe, bias in abundance, bias in bycatch
##
## Authors : Matthieu Authier & Mathieu Genu
## Last update : 2021-07-10
## R version 4.0.4 (2021-02-15) -- "Lost Library Book"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

q <- 0.3
Kfinal <- 1
cata <- 0
bias_abund <- 1.5
bias_byc <- 1.5

source("tuningThor.r")
