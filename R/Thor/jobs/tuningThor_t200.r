##--------------------------------------------------------------------------------------------------------
## SCRIPT : tuning RLA
##
## Authors : Matthieu Authier, Mathieu Genu, Phil Hammond
## Last update : 2021-07-10
## R version 4.0.4 (2021-02-15) -- "Lost Library Book"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

# install.packages()
lapply(c("tidyverse", "rstan", "RLA", "MCMCpack"), library, character.only = TRUE)

options(mc.cores = parallel::detectCores(), warn = -1)
rstan::rstan_options(auto_write = TRUE)

data("north_sea_hp")
data("scenarios_north_sea_hp")
load("rlastan.RData")
load("20210710_HPNS_OMbase.RData")

tuning <- lapply(rla, function(x) {
  forward_rla(rlalist = x,
              rlastan = rlastan,
              q = q,
              # bycatch-related parameters
              bycatch_variation_cv = 0.3,
              bycatch_error_cv = c(0.3, 0),
              distribution = "truncnorm",
              # upper bound for the prior on r, the population growth rate
              upper = 0.1,
              # robustness checks
              bias_abund = bias_abund,
              Ktrend = Kfinal,
              catastrophe = cata,
              bias_byc = bias_byc,
              random = FALSE,
              save_log = FALSE,
              horizon = 200
              )
})

save(file = paste("HPNS_", 
                  ifelse(bias_abund == 1, 0, 1), "_", 
                  ifelse(bias_byc == 1, 0, 1), "_", 
                  ifelse(Kfinal == 1, 0, 1), "_", 
                  10 * cata, 
                  10 * q, 
                  "_t200.RData", sep = ""
                  ), 
	 list = c("tuning", "q", "Kfinal", "cata", "bias_byc", "bias_abund")
	 )
