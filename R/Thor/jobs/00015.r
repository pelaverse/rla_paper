##--------------------------------------------------------------------------------------------------------
## SCRIPT : quantile 50%, K constant, catastrophe of 20%, no bias
##
## Authors : Matthieu Authier, Mathieu Genu & Phil Hammond
## Last update : 2021-07-10
## R version 4.0.4 (2021-02-15) -- "Lost Library Book"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

q <- 0.5
Kfinal <- 1
cata <- 0.2
bias_abund <- 1
bias_byc <- 1

source("tuningThor.r")
