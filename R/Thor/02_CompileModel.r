library(RLA)
library(rstan)

## load Stan models
data(rlastan_models)

# compile model
rlastan <- rstan::stan_model(model_code = rlastan_models$uniform,
                             model_name = "RLA"
                             )
                             
save(list = "rlastan", file = "rlastan.RData")