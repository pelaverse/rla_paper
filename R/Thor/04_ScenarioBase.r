##--------------------------------------------------------------------------------------------------------
## SCRIPT : Scenarios with complete ban
##
## Authors : Matthieu Authier, Mathieu Genu
## Last update : 2021-08-16
##
## R version 4.0.4 (2021-02-15) -- "Lost Library Book"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("RLA", "rstan", "tidyverse", "ggthemes", "MCMCpack"),
       library, character.only = TRUE
       )

rm(list = ls())

param <- read.table("20210710_HPNS_OMbase.txt", header = TRUE)
load("20210710_HPNS_OMbase.RData")

# pairs(param[, c(1, 5:9)])
ban <- lapply(rla, zero_mortality)

### load results from base case scenario
get_management_results <- function(scenario, path_results, param) {
  load(paste0(path_results, "/HPNS_", scenario, ".RData"))
  out <- do.call('rbind', lapply(1:length(tuning), 
                                 function(i) { 
                                   tuning[[i]]$management %>% 
                                     mutate(t = 1:n(), 
                                            sim = i
                                            ) 
                                   }
                                 )
                 ) %>%
    left_join(param,
              by = "seed"
              )
  return(out)
}

get_trajectory <- function(scenario, path_results, param) {
  load(paste0(path_results, "/HPNS_", scenario, ".RData"))
  out <- do.call('rbind', lapply(1:length(tuning), 
                                 function(i) { 
                                   tuning[[i]]$depletion %>% 
                                     mutate(sim = i) 
                                 }
                                 )
                 ) %>%
    left_join(param,
              by = "seed"
              )
  return(out)
}
### base case scenario
all_management_results <- do.call('rbind', lapply(paste0("bis0_0_0_0", 2:8), 
                                                  get_management_results, 
                                                  path_results = "res/RLA",
                                                  param = param
                                                  )
                                  )
all_trajectories <- do.call('rbind', lapply(paste0("bis0_0_0_0", 2:8), 
                                            get_trajectory, 
                                            path_results = "res/RLA",
                                            param = param
                                            )
                            )

### some plots
all_management_results %>% 
  mutate(bycatch = catch_limit * N_hat) %>%
  filter(t == last(t)) %>%
  group_by(quantile) %>% # , MNPL_cat, level
  summarize(conservation = mean(ifelse(depletion > 0.8, 1, 0)),
            depletion = mean(depletion)
            ) %>% 
  View()

all_management_results %>% 
  mutate(bycatch = catch_limit * N_hat) %>%
  filter(t == last(t)) %>%
  group_by(quantile, level) %>% # , MNPL_cat, level
  summarize(conservation = mean(ifelse(depletion > 0.8, 1, 0)),
            depletion = mean(depletion)
            ) %>% 
  View()

resume <- all_management_results %>% 
  mutate(bycatch = catch_limit * N_hat) %>%
  group_by(quantile) %>%
  summarize(mean_rate = mean(catch_limit), 
            mean_bycatch = mean(bycatch),
            max_bycatch = max(bycatch),
            q80 = quantile(bycatch, probs = 0.80),
            q95 = quantile(bycatch, probs = 0.95),
            ) %>% 
  left_join(all_management_results %>% 
              mutate(bycatch = catch_limit * N_hat) %>%
              filter(t == last(t)) %>%
              group_by(quantile) %>% # , MNPL_cat, level
              summarize(conservation = mean(ifelse(depletion > 0.8, 1, 0)),
                        lower = quantile(depletion, probs = 0.025),
                        upper = quantile(depletion, probs = 0.975),
                        depletion = mean(depletion)
                        ),
            by = c("quantile")
            )

resume %>% 
  View()

resume %>% 
  ggplot() +
  geom_hline(yintercept = 0.8, linetype = "dotted", color = "tomato", size = 2) +
  geom_line(aes(x = quantile, y = conservation)) +
  ylab("Pr(> 80% K)") +
  theme_bw()

resume %>% 
  ggplot() +
  geom_hline(yintercept = 0.8, linetype = "dotted", color = "tomato", size = 2) +
  geom_ribbon(aes(x = quantile, ymin = lower, ymax = upper), alpha = 0.3) +
  geom_line(aes(x = quantile, y = depletion)) +
  ylab("Depletion after 100 years\n(as % of K)") +
  theme_bw()

### some plots
ban_results <- do.call('rbind', lapply(1:length(ban), 
                                       function(i) { 
                                         ban[[i]]$depletion %>% 
                                           mutate(sim = i)
                                         }
                                       )
                       ) %>%
  left_join(param,
            by = "seed"
            ) %>% 
  group_by(time, level) %>% 
  summarize(mean = mean(depletion),
            lower = as.numeric(quantile(depletion, probs = 0.025)),
            upper = as.numeric(quantile(depletion, probs = 0.975))
            )
ban_results

# first match with counterfactual of a complete ban
counterfactual <- do.call('rbind', 
                          lapply(1:length(ban), function(i) { 
                            ban[[i]]$depletion %>% 
                              mutate(sim = i)
                              }
                                 )
                          ) %>%
  left_join(param,
            by = "seed"
            ) %>% 
  dplyr::select(MNP, level, MNPL_cat, seed, time, depletion, sim) %>% 
  rename(recovery = depletion)

counterfactual <- do.call('rbind', 
                          lapply(1:length(rla), function(i) { 
                            ban[[i]]$depletion %>% 
                              mutate(sim = i)
                            }
                            )
                          ) %>%
  left_join(param,
            by = "seed"
            ) %>% 
  dplyr::select(MNP, level, MNPL_cat, seed, time, depletion, sim) %>% 
  rename(recovery = depletion)

performance <- all_trajectories %>% 
  dplyr::select(quantile, MNP, level, MNPL_cat, seed, time, depletion, sim, removals) %>% 
  left_join(counterfactual,
            by = c("MNP", "level", "MNPL_cat", "seed", "time", "sim")
            ) %>% 
  group_by(quantile, MNP, level, MNPL_cat, seed) %>% 
  mutate(ban = ifelse(recovery > 0.8, 1, 0),
         rla = ifelse(depletion > 0.8, 1, 0)
         ) %>% 
  group_by(quantile, MNP, level, MNPL_cat, seed) %>% 
  summarize(time1 = time2CO(ban),
            time2 = time2CO(rla)
            ) %>% 
  mutate(delta = time1 - time2) # ok if < 0, if faster recovery with a ban

#########################
## Performance metrics ##
#########################

### delay in recovery
performance %>% 
  group_by(quantile, level) %>% 
  summarize(recovery_delay = mean(delta, na.rm = TRUE),
            lower = quantile(delta, probs = 0.025, na.rm = TRUE),
            upper = quantile(delta, probs = 0.975, na.rm = TRUE),
            sample_size = sum(!is.na(delta))
            ) %>% 
  ggplot() +
  geom_hline(yintercept = 0.0, color = "tomato", linetype = "dotted", size = 2) +
  geom_ribbon(aes(x = quantile, ymin = lower, ymax = upper), 
              fill = "midnightblue",
              alpha = 0.3
              ) +
  geom_line(aes(x = quantile, y = recovery_delay), 
            color = "midnightblue", size = 2
            ) +
  scale_x_continuous(name = "Quantile", breaks = seq(0.2, 0.8, 0.1)) +
  scale_y_continuous(name = "Delay (in years) in recovery\ncompared to a ban",
                     breaks = seq(0, -100, -10)
                     ) +
  facet_wrap(~level, ncol = 3) +
  ggtitle("True bycatch underestimated by half", 
          subtitle = "MNP = 4%, average MNPL = 60%, initial depletion = 30-90%"
          ) +
  theme_bw()

### average depletion level
resume %>% 
  ggplot() +
  geom_hline(yintercept = 0.8, linetype = "dotted", color = "tomato", size = 2) +
  geom_ribbon(aes(x = quantile, ymin = lower, ymax = upper), alpha = 0.3, fill = "midnightblue") +
  geom_line(aes(x = quantile, y = depletion), color = "midnightblue", size = 2) +
  scale_x_continuous(name = "Quantile", breaks = seq(0.2, 0.8, 0.1)) +
  ylab("Depletion after 100 years\n(as % of K)") +
  ggtitle("True bycatch underestimated by half", 
          subtitle = "MNP = 4%, average MNPL = 60%, initial depletion = 30-90%"
          ) +
  theme_bw()

### probability to reach CO
resume %>% 
  ggplot() +
  geom_line(aes(x = quantile, y = conservation), color = "midnightblue", size = 2) +
  geom_hline(yintercept = 0.8, color = "tomato", linetype = "dotted") +
  scale_y_continuous(name = "Probability of reaching the conservation objective\n(population is at > 80% of carrying capacity)",
                     breaks = seq(0, 1, 0.05)
                     ) +
  scale_x_continuous(name = "Quantile", breaks = seq(0.2, 0.9, 0.1)) +
  ggtitle("Survey every 12 years", 
          subtitle = "MNP = 4%, average MNPL = 60%, initial depletion = 30-90"
          ) +
  theme_bw()

### simulations
ggplot() +
  geom_line(data = ban_results,
            aes(x = time, y = mean, group = level), 
            linetype = "dashed", color = "black", size = 1
            ) +
  geom_hline(yintercept = 0.8, color = "tomato", linetype = "dotted", size = 2) +
  geom_line(data = all_trajectories %>% 
              filter(quantile == 0.7),
            aes(x = time, y = depletion, group = factor(seed * quantile)), alpha = 0.05
            ) +
  geom_line(data = all_trajectories %>% 
              filter(quantile == 0.7) %>% 
              group_by(time, level) %>% 
              summarize(mean = mean(depletion),
                        lower = as.numeric(quantile(depletion, probs = 0.025)),
                        upper = as.numeric(quantile(depletion, probs = 0.975))
                        ),
            aes(x = time, y = mean, group = level, color = level), size = 1
            ) +
  scale_y_continuous(name = "Depletion (as % of K)",
                     breaks = seq(0.2, 1.2, 0.1),
                     labels = 100 * seq(0.2, 1.2, 0.1)
                     ) +
  ggtitle("Survey every 12 years", 
          subtitle = "MNP = 4%, Quantile = 60%"
          ) +
  scale_color_brewer(type = "seq") +
  # guides(color = "none") +
  theme_bw()


### performance
performance %>% 
  group_by(quantile) %>% 
  summarize(recovery_delay = mean(delta, na.rm = TRUE),
            lower = quantile(delta, probs = 0.025, na.rm = TRUE),
            upper = quantile(delta, probs = 0.975, na.rm = TRUE),
            sample_size = sum(!is.na(delta))
            )
### depletion at t = 20 and t = 50
all_trajectories %>% 
  filter(quantile == 0.7,
         time %in% c(20, 50)
         ) %>% 
  group_by(level, time) %>% 
  summarize(smaple_size = n(),
            depletion = mean(depletion)
            )
