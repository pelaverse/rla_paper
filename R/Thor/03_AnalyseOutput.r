##--------------------------------------------------------------------------------------------------------
## SCRIPT : Scenarios with complete ban
##
## Authors : Matthieu Authier, Mathieu Genu
## Last update : 2021-08-09
##
## R version 4.0.4 (2021-02-15) -- "Lost Library Book"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("RLA", "rstan", "tidyverse", "ggthemes", "MCMCpack"),
       library, character.only = TRUE
       )

rm(list = ls())

param <- read.table("20210710_HPNS_OMbase.txt", header = TRUE)
load("20210710_HPNS_OMbase.RData")


# normal autour de MNPL centré en 0.6
param %>% 
  ggplot(aes(x = MNPL)) +
  geom_histogram(bins = 25, colour = "black", fill = "grey60") +
  theme_bw()


# pairs(param[, c(1, 5:9)])
ban <- lapply(rla, zero_mortality)
ban_t50 <- lapply(rla, zero_mortality, horizon = 50)
ban_t200 <- lapply(rla, zero_mortality, horizon = 200)

### degradation of K
Kban <- lapply(rla, zero_mortality, Ktrend = 0.5)

### catastrophic mortality event
cban <- lapply(rla, zero_mortality, catastrophe = 0.1)

### allow for some uncertainty in estimate when selecting a quantile
tolerance <- 0.01

### load results from base case scenario

get_management_results <- function(scenario, path_results, param) {
  
  load(paste0(path_results, "/HPNS_", scenario, ".RData"))
  
  out <- tuning %>%
    map("management") %>%  
    map_dfr(rbind, .id = "sim") %>% 
    mutate(t = seq(1:nrow(tuning[[1]]$management)) %>% rep(length(tuning))) %>% 
    left_join(param, by = "seed")
  
  return(out)
}


get_trajectory <- function(scenario, path_results, param) {
  
  load(paste0(path_results, "/HPNS_", scenario, ".RData"))
  
  out <- tuning %>%
    map("depletion") %>%  
    map_dfr(rbind, .id = "sim") %>% 
    mutate(t = seq(1:nrow(tuning[[1]]$depletion)) %>% rep(length(tuning))) %>% 
    left_join(param, by = "seed")
  
  return(out)
  
}



### base case scenario


df_scenarios <- data.frame(
  code = c("0_0_0_0","1_0_0_0","1_1_0_0","0_1_0_0","0_0_0_2",
           "0_0_1_0", "bis0_0_0_0","bis1_1_0_0","0_0_0_0_t50","0_0_0_0_t200"),
  name = c("base", "abun", "abyc", "byc", "cata", "half", "base12", "abyc12","base_t50","base_t200"),
  title = c("Base scenario",
            "abundance is overestimated by 2",
            "abundance is overestimated by 1.5 and bycatch is underestimated by 1.5",
            "bycatch is underestimated by 2",
            "catastrophic mortality event wiping out 20% of individuals",
            "carrying capacity in progressively halved over 100 years",
            "survey every 12 years",
            "overestimated abundance, underestimated bycatch, survey every 12 years",
            "Base scenario with a 50 years horizon",
            "Base scenario with a 200 years horizon"
            )
)

tot_depletion_time <- NULL
tot_perf <- NULL
complete_perf <- NULL

for(s in 1:nrow(df_scenarios)) {
  
  sc <- df_scenarios$name[s]
  cli::cli_h1("{.sc {sc}}")
  scenario <- 2:8
  
  if(df_scenarios$code[s] %in% c("1_1_0_0","0_0_0_0_t200")) {
    scenario <- 2:7
  }
  if(df_scenarios$code[s] == "bis1_1_0_0") {
    scenario <- 2:6
  }
  
  
  if(df_scenarios$name[s] %in% c("base_t50","base_t200")) {
    
    name_split <- df_scenarios$name[s] %>% strsplit("_") %>% unlist()
    
    all_management_results <- paste(paste0("0_0_0_0",scenario), name_split[2], sep="_") %>% 
      map(get_management_results, 
          path_results = "res/RLA",
          param = param) %>% 
      map_dfr(rbind)
    
    
    all_trajectories <- paste(paste0("0_0_0_0",scenario), name_split[2], sep="_") %>% 
      map(get_trajectory, 
          path_results = "res/RLA",
          param = param) %>% 
      map_dfr(rbind)
    
  } else {
    
    all_management_results <- paste0(df_scenarios$code[s], scenario) %>% 
      map(get_management_results, 
          path_results = "res/RLA",
          param = param) %>% 
      map_dfr(rbind)
    
    
    all_trajectories <- paste0(df_scenarios$code[s], scenario) %>% 
      map(get_trajectory, 
          path_results = "res/RLA",
          param = param) %>% 
      map_dfr(rbind)
    
  }

  
  
  ### some plots
  all_management_results %>% 
    mutate(bycatch = catch_limit * N_hat) %>%
    filter(t == last(t)) %>%
    group_by(quantile) %>% # , MNPL_cat, level
    summarize(conservation = mean(ifelse(depletion > 0.8, 1, 0)),
              depletion = mean(depletion),
              .groups = "drop"
              ) %>% 
    View()
  
  resume <- all_management_results %>% 
    mutate(bycatch = catch_limit * N_hat) %>%
    group_by(quantile) %>%
    summarize(mean_rate = mean(catch_limit), 
              mean_bycatch = mean(bycatch),
              max_bycatch = max(bycatch),
              q80 = quantile(bycatch, probs = 0.80),
              q95 = quantile(bycatch, probs = 0.95),
              .groups = "drop"
              ) %>% 
    left_join(all_management_results %>% 
                mutate(bycatch = catch_limit * N_hat) %>%
                filter(t == last(t)) %>%
                group_by(quantile) %>% # , MNPL_cat, level
                summarize(conservation = mean(ifelse(depletion > 0.8, 1, 0)),
                          lower = quantile(depletion, probs = 0.025),
                          upper = quantile(depletion, probs = 0.975),
                          depletion = mean(depletion),
                          .groups = "drop"
                          ),
              by = c("quantile")
              )
  
  resume %>% 
    View()
  
  ### keep results stratified by initial depletion level
  complete_resume <- all_management_results %>% 
    mutate(bycatch = catch_limit * N_hat) %>%
    group_by(quantile, level) %>%
    summarize(mean_rate = mean(catch_limit), 
              mean_bycatch = mean(bycatch),
              max_bycatch = max(bycatch),
              q80 = quantile(bycatch, probs = 0.80),
              q95 = quantile(bycatch, probs = 0.95),
              .groups = "drop"
              ) %>% 
    left_join(all_management_results %>% 
                mutate(bycatch = catch_limit * N_hat) %>%
                filter(t == last(t)) %>%
                group_by(quantile, level) %>% # , MNPL_cat, level
                summarize(conservation = mean(ifelse(depletion > 0.8, 1, 0)),
                          lower = quantile(depletion, probs = 0.025),
                          upper = quantile(depletion, probs = 0.975),
                          depletion = mean(depletion),
                          .groups = "drop"
                          ),
              by = c("quantile", "level")
              )
  # quantile choice
  quant_scenario <- max(resume$quantile[resume$conservation >= 0.80 - tolerance])
  
  # resume %>% 
  #   write.table(file = "BiasedBycatch.txt", quote = FALSE, sep = "\t", # changer titre du fichier
  #               row.names = FALSE, col.names = TRUE
  #               )
  
  resume %>% 
    ggplot() +
    geom_hline(yintercept = 0.8, linetype = "dotted", color = "tomato", size = 2) +
    geom_line(aes(x = quantile, y = conservation)) +
    ylab("Pr(> 80% K)") +
    theme_bw()
  
  resume %>% 
    ggplot() +
    geom_hline(yintercept = 0.8, linetype = "dotted", color = "tomato", size = 2) +
    geom_ribbon(aes(x = quantile, ymin = lower, ymax = upper), alpha = 0.3) +
    geom_line(aes(x = quantile, y = depletion)) +
    ylab("Depletion after 100 years\n(as % of K)") +
    theme_bw()
  
  ### some plots
  if(df_scenarios$name[s] == "base_t50") {
    completeban <- ban_t50
  } else if(df_scenarios$name[s] == "base_t200") {
    completeban <- ban_t200
  }else {
    completeban <- ban 
  }
  if(df_scenarios$code[s] == "0_0_0_2") {
    completeban <- cban
  }
  if (df_scenarios$code[s] == "0_0_1_0") {
    completeban <- Kban
  }
  ban_results <- completeban %>%
    map_dfr("depletion", rbind, .id = "sim") %>% 
    left_join(param,by = "seed") %>% 
    group_by(time, level) %>% 
    summarize(mean = mean(depletion),
              lower = as.numeric(quantile(depletion, probs = 0.025)),
              upper = as.numeric(quantile(depletion, probs = 0.975)),
              .groups = "drop"
              )
  ban_results
  
  # first match with counterfactual of a complete ban
  
  counterfactual <- completeban %>% 
    map("depletion") %>%
    map_dfr(rbind, .id = "sim") %>% 
    left_join(param, by = "seed") %>% 
    dplyr::select(MNP, level, MNPL_cat, seed, time, depletion, sim) %>% 
    rename(recovery = depletion)
  
  counterfactual <- completeban %>% 
    map("depletion") %>%
    map_dfr(rbind, .id = "sim") %>% 
    left_join(param, by = "seed") %>% 
    dplyr::select(MNP, level, MNPL_cat, seed, time, depletion, sim) %>% 
    rename(recovery = depletion)
  
  
  performance <- all_trajectories %>% 
    dplyr::select(quantile, MNP, level, MNPL_cat, seed, time, depletion, sim, removals) %>% 
    left_join(counterfactual,
              by = c("MNP", "level", "MNPL_cat", "seed", "time", "sim")
              ) %>% 
    group_by(quantile, MNP, level, MNPL_cat, seed) %>% 
    mutate(ban = ifelse(recovery > 0.8, 1, 0),
           rla = ifelse(depletion > 0.8, 1, 0)
           ) %>% 
    group_by(quantile, MNP, level, MNPL_cat, seed) %>% 
    summarize(time1 = time2CO(ban),
              time2 = time2CO(rla),
              .groups = "drop"
              ) %>% 
    mutate(delta = time1 - time2) # ok if < 0: faster recovery with a ban
  
  #########################
  ## Performance metrics ##
  #########################
  
  ### delay in recovery
  performance %>% 
    group_by(quantile, level) %>% 
    summarize(recovery_delay = mean(delta, na.rm = TRUE),
              lower = quantile(delta, probs = 0.025, na.rm = TRUE),
              upper = quantile(delta, probs = 0.975, na.rm = TRUE),
              sample_size = sum(!is.na(delta)),
              .groups = "drop"
              ) %>% 
    ggplot() +
    geom_hline(yintercept = 0.0, color = "tomato", linetype = "dotted", size = 2) +
    geom_ribbon(aes(x = quantile, ymin = lower, ymax = upper), 
                fill = "midnightblue",
                alpha = 0.3
                ) +
    geom_line(aes(x = quantile, y = recovery_delay), 
              color = "midnightblue", size = 2
              ) +
    scale_x_continuous(name = "Quantile", breaks = seq(0.2, 0.8, 0.1)) +
    scale_y_continuous(name = "Delay (in years) in recovery\ncompared to a ban",
                       breaks = seq(0, -100, -10)
                       ) +
    facet_wrap(~level, ncol = 3) +
    ggtitle(glue::glue(df_scenarios$title[s]), # titre à changer
            subtitle = "MNP = 4%, average MNPL = 60%, initial depletion = 30-90%"
            ) +
    theme_bw()
  ggsave(filename = paste("res/RLA", paste0(df_scenarios$name[s],"_performance.png"), sep = "/"),  # titre à changer
         width = 15, height = 12, units = "cm", dpi = 300
         )
  
  complete_resume <- complete_resume %>% 
    left_join(performance %>% 
                group_by(quantile, level) %>% 
                summarize(recovery_delay = mean(delta, na.rm = TRUE),
                          lower_delay = quantile(delta, probs = 0.025, na.rm = TRUE),
                          upper_delay = quantile(delta, probs = 0.975, na.rm = TRUE),
                          sample_size = sum(!is.na(delta)),
                          .groups = "drop"
                ),
              by = c("quantile", "level")
              )
  ### average depletion level
  resume %>% 
    ggplot() +
    geom_hline(yintercept = 0.8, linetype = "dotted", color = "tomato", size = 2) +
    geom_ribbon(aes(x = quantile, ymin = lower, ymax = upper), alpha = 0.3, fill = "midnightblue") +
    geom_line(aes(x = quantile, y = depletion), color = "midnightblue", size = 2) +
    scale_x_continuous(name = "Quantile", breaks = seq(0.2, 0.8, 0.1)) +
    ylab("Depletion after 100 years\n(as % of K)") +
    ggtitle(glue::glue(df_scenarios$title[s]), 
            subtitle = "MNP = 4%, average MNPL = 60%, initial depletion = 30-90%"
            ) +
    theme_bw()
  ggsave(filename = paste("res/RLA", paste0(df_scenarios$name[s],"_depletion.png"), sep = "/"),
         width = 15, height = 12, units = "cm", dpi = 300
         )
  ### probability to reach CO
  resume %>% 
    ggplot() +
    geom_line(aes(x = quantile, y = conservation), color = "midnightblue", size = 2) +
    geom_hline(yintercept = 0.8, color = "tomato", linetype = "dotted") +
    scale_y_continuous(name = "Probability of reaching the conservation objective\n(population is at > 80% of carrying capacity)",
                       breaks = seq(0, 1, 0.05)
                       ) +
    scale_x_continuous(name = "Quantile", breaks = seq(0.2, 0.9, 0.1)) +
    ggtitle(glue::glue(df_scenarios$title[s]), 
            subtitle = "MNP = 4%, average MNPL = 60%, initial depletion = 30-90"
            ) +
    theme_bw()
  ggsave(filename = paste("res/RLA", paste0(df_scenarios$name[s],"_proba.png"), sep = "/"),
         width = 15, height = 12, units = "cm", dpi = 300
         )
  ### simulations
  ggplot() +
    geom_line(data = ban_results,
              aes(x = time, y = mean, group = level), 
              linetype = "dashed", color = "black", size = 1
              ) +
    geom_hline(yintercept = 0.8, color = "tomato", linetype = "dotted", size = 2) +
    geom_line(data = all_trajectories %>% 
                filter(quantile == quant_scenario),
              aes(x = time, y = depletion, group = factor(seed * quantile)), alpha = 0.05
              ) +
    geom_line(data = all_trajectories %>% 
                filter(quantile == quant_scenario) %>% 
                group_by(time, level) %>% 
                summarize(mean = mean(depletion),
                          lower = as.numeric(quantile(depletion, probs = 0.025)),
                          upper = as.numeric(quantile(depletion, probs = 0.975)),
                          .groups = "drop"
                          ),
              aes(x = time, y = mean, group = level, color = level), size = 1
              ) +
    scale_y_continuous(name = "Depletion (as % of K)",
                       breaks = seq(0.2, 1.2, 0.1),
                       labels = 100 * seq(0.2, 1.2, 0.1)
                       ) +
    ggtitle(glue::glue({df_scenarios$title[s]}), 
            subtitle = glue::glue("MNP = 4%, Quantile = {quant_scenario*100}%")
            ) +
    scale_color_brewer(type = "seq") +
    # guides(color = "none") +
    theme_bw()
  ggsave(filename = paste("res/RLA", paste0(df_scenarios$name[s],"_simul.png"), sep = "/"),
         width = 15, height = 12, units = "cm", dpi = 300
         )
  
  ### performance
  tmp_pref <- performance %>% 
    group_by(quantile) %>% 
    summarize(recovery_delay = mean(delta, na.rm = TRUE),
              lower = quantile(delta, probs = 0.025, na.rm = TRUE),
              upper = quantile(delta, probs = 0.975, na.rm = TRUE),
              sample_size = sum(!is.na(delta)),
              .groups = "drop"
              ) %>% 
    mutate(scenario = df_scenarios$name[s]) %>% 
    filter(quantile == quant_scenario)
  
  
  tot_perf <- rbind(tot_perf, tmp_pref)
  
  ### depletion at t = 20 and t = 50
  tmp_depletion_time <- all_trajectories %>% 
    filter(quantile == quant_scenario,
           time %in% c(20, 50, 100)
           ) %>% 
    mutate(quantile = quant_scenario,
           scenario = df_scenarios$name[s]) %>% 
    group_by(time, quantile, scenario) %>% 
    summarize(smaple_size = n(),
              depletion = mean(depletion),
              .groups = "drop"
              )
  
  tot_depletion_time <- rbind(tmp_depletion_time, tot_depletion_time)
  
  complete_perf <- rbind(complete_perf,
                         complete_resume %>% 
                           mutate(scenario = df_scenarios$name[s])
                         )
}

# save.image("c:/Users/mgenu.RATUFA/Desktop/tmp_RLA_sim.RData")
load("c:/Users/mgenu.RATUFA/Desktop/tmp_RLA_sim.RData")

tot_perf %>% 
  write.table(file = "res/RLA/perf_tot.txt",
              quote = FALSE, 
              sep = "\t", 
              row.names = FALSE,
              col.names = TRUE
              )

tot_depletion_time %>% 
  write.table(file = "res/RLA/depletion_time_tot.txt",
              quote = FALSE, 
              sep = "\t", 
              row.names = FALSE,
              col.names = TRUE
              )

complete_perf %>% 
  write.table(file = "res/RLA/performance_complete.txt",
              quote = FALSE, 
              sep = "\t", 
              row.names = FALSE,
              col.names = TRUE
              )


tot_perf %>% 
  dplyr::select(quantile, scenario) %>% 
  left_join(complete_perf, by = c("scenario","quantile")) %>% 
  ggplot() +
  geom_boxplot(aes(x = scenario, y = mean_rate))
  
  tot_perf %>% dplyr::select(quantile, scenario)
  
  
  
  
  
complete_perf %>% 
  group_by(scenario) %>% 
  summarize(mean_byc = mean(mean_bycatch),
            med = median(mean_bycatch))
