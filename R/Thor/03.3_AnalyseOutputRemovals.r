
lapply(c("RLA", "rstan", "tidyverse", "ggthemes", "MCMCpack"),
       library, character.only = TRUE
)

rm(list = ls())

param <- read.table("20210710_HPNS_OMbase.txt", header = TRUE)
load("20210710_HPNS_OMbase.RData")


# normal autour de MNPL centré en 0.6
param %>% 
  ggplot(aes(x = MNPL)) +
  geom_histogram(bins = 25, colour = "black", fill = "grey60") +
  theme_bw()


# pairs(param[, c(1, 5:9)])
ban <- lapply(rla, zero_mortality)

### degradation of K
Kban <- lapply(rla, zero_mortality, Ktrend = 0.5)

### catastrophic mortality event
cban <- lapply(rla, zero_mortality, catastrophe = 0.1)

### allow for some uncertainty in estimate when selecting a quantile
tolerance <- 0.01

### load results from base case scenario

get_management_results <- function(scenario, path_results, param) {
  
  load(paste0(path_results, "/HPNS_", scenario, ".RData"))
  
  out <- tuning %>%
    map("management") %>%  
    map_dfr(rbind, .id = "sim") %>% 
    mutate(t = seq(1:nrow(tuning[[1]]$management)) %>% rep(length(tuning))) %>% 
    left_join(param, by = "seed")
  
  return(out)
}


get_trajectory <- function(scenario, path_results, param) {
  
  load(paste0(path_results, "/HPNS_", scenario, ".RData"))
  
  out <- tuning %>%
    map("depletion") %>%  
    map_dfr(rbind, .id = "sim") %>% 
    mutate(t = seq(1:nrow(tuning[[1]]$depletion)) %>% rep(length(tuning))) %>% 
    left_join(param, by = "seed")
  
  return(out)
  
}



tot_complete_resume <- NULL
tot_all_management <- NULL

### base case scenario


df_scenarios <- data.frame(
  code = c("0_0_0_0","1_0_0_0","1_1_0_0","0_1_0_0","0_0_0_2","0_0_1_0", "bis0_0_0_0","bis1_1_0_0"),
  name = c("base", "abun", "abyc", "byc", "cata", "half", "base12", "abyc12"),
  title = c("Base scenario",
            "abundance is overestimated by 2",
            "abundance is overestimated by 1.5 and bycatch is underestimated by 1.5",
            "bycatch is underestimated by 2",
            "catastrophic mortality event wiping out 20% of individuals",
            "carrying capacity in progressively halved over 100 years",
            "survey every 12 years",
            "overestimated abundance, underestimated bycatch, survey every 12 years"
  )
)


for(s in 1:nrow(df_scenarios)) {
  
  sc <- df_scenarios$name[s]
  cli::cli_h1("{.sc {sc}}")
  scenario <- 2:8
  
  if(df_scenarios$code[s] == "1_1_0_0") {
    scenario <- 2:7
  }
  if(df_scenarios$code[s] == "bis1_1_0_0") {
    scenario <- 2:6
  }
  
  all_management_results <- paste0(df_scenarios$code[s], scenario) %>% 
    map(get_management_results, 
        path_results = "res/RLA",
        param = param) %>% 
    map_dfr(rbind) %>% 
    mutate(bycatch = catch_limit * N_hat,
           name = df_scenarios$name[s])
  
  
  all_trajectories <- paste0(df_scenarios$code[s], scenario) %>% 
    map(get_trajectory, 
        path_results = "res/RLA",
        param = param) %>% 
    map_dfr(rbind)
  
  
  ### some plots
  # all_management_results %>% 
  #   mutate(bycatch = catch_limit * N_hat) %>%
  #   filter(t == last(t)) %>%
  #   group_by(quantile) %>% # , MNPL_cat, level
  #   summarize(conservation = mean(ifelse(depletion > 0.8, 1, 0)),
  #             depletion = mean(depletion),
  #             .groups = "drop"
  #   ) 
  
  resume <- all_management_results %>% 
    mutate(bycatch = catch_limit * N_hat) %>%
    group_by(quantile) %>%
    summarize(mean_rate = mean(catch_limit), 
              mean_bycatch = mean(bycatch),
              max_bycatch = max(bycatch),
              q80 = quantile(bycatch, probs = 0.80),
              q95 = quantile(bycatch, probs = 0.95),
              .groups = "drop"
    ) %>% 
    left_join(all_management_results %>% 
                mutate(bycatch = catch_limit * N_hat) %>%
                filter(t == last(t)) %>%
                group_by(quantile) %>% # , MNPL_cat, level
                summarize(conservation = mean(ifelse(depletion > 0.8, 1, 0)),
                          lower = quantile(depletion, probs = 0.025),
                          upper = quantile(depletion, probs = 0.975),
                          depletion = mean(depletion),
                          .groups = "drop"
                ),
              by = c("quantile")
    )
  
  # resume %>% 
  #   View()
  
  # quantile choice
  quant_scenario <- max(resume$quantile[resume$conservation >= 0.80 - tolerance])
  
  ### keep results stratified by initial depletion level
  complete_resume <- all_management_results %>% 
    mutate(bycatch = catch_limit * N_hat) %>%
    group_by(quantile, level) %>%
    summarize(mean_rate = mean(catch_limit), 
              mean_bycatch = mean(bycatch),
              max_bycatch = max(bycatch),
              q80 = quantile(bycatch, probs = 0.80),
              q95 = quantile(bycatch, probs = 0.95),
              .groups = "drop"
    ) %>% 
    left_join(all_management_results %>% 
                mutate(bycatch = catch_limit * N_hat) %>%
                filter(t == last(t)) %>%
                group_by(quantile, level) %>% # , MNPL_cat, level
                summarize(conservation = mean(ifelse(depletion > 0.8, 1, 0)),
                          lower = quantile(depletion, probs = 0.025),
                          upper = quantile(depletion, probs = 0.975),
                          depletion = mean(depletion),
                          .groups = "drop"
                ),
              by = c("quantile", "level")
    ) %>% 
    mutate(name = df_scenarios$name[s],
           quantile_to_keep = quant_scenario)
  
  
  all_management_results <- all_management_results %>% 
    mutate(quantile_to_keep = quant_scenario)
  
  tot_complete_resume <- tot_complete_resume %>% 
    bind_rows(complete_resume)
  
  tot_all_management <- tot_all_management %>% 
    bind_rows(all_management_results)
  
}


tot_all_management %>% 
  filter(quantile == quantile_to_keep,
         time == last(time)) %>% 
  group_by(quantile, name) %>% 
  summarize(mean_catch_limit = mean(catch_limit),
            error_catch_limit = sd(catch_limit)) %>% 
  ggplot() +
  geom_point(aes(x = name, y = mean_catch_limit, colour = name)) +
  geom_linerange(aes(x = name, 
                    ymin = mean_catch_limit - error_catch_limit, 
                    ymax = mean_catch_limit + error_catch_limit,
                    colour = name)) +
  theme_bw()


tot_all_management %>% 
  filter(quantile == quantile_to_keep,
         time == last(time)) %>% 
  ggplot() +
  geom_violin(aes(x = name, y = bycatch, colour = name, fill = name)) +
  theme_bw() +
  labs(title = "Bycatch distribution per trials",
       subtitle = "Estimated for last Scans survey")
