##--------------------------------------------------------------------------------------------------------
## SCRIPT : SOme plots of inputs
##
## Authors : Matthieu Authier, Mathieu Genu
## Last update : 2021-07-18
##
## R version 4.0.4 (2021-02-15) -- "Lost Library Book"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("RLA", "rstan", "tidyverse", "ggthemes", "ggExtra"),
       library, character.only = TRUE
       )

rm(list = ls())

param <- read.table("20210710_HPNS_OMbase.txt", header = TRUE)

# Save the scatter plot in a variable
m <- ggplot(data = param, 
            aes(x = current_depletion, y = MNPL)
            ) +
  geom_point() +
  geom_smooth(method = "loess", se = FALSE, col = "midnightblue") +
  scale_x_continuous(name = "Initial depletion (% K)",
                     breaks = seq(0.3, 0.9, 0.1)
                     ) +
  scale_y_continuous(name = "Maximum Net Productivity Level (% K)",
                     breaks = seq(0.45, 0.80, 0.05)
                     ) +
  theme_bw()

# Densigram
m <- ggMarginal(m, 
                type = "densigram",
                fill = "white",
                col = 4
                )
ggsave(m,
       filename = paste("res/RLA", "Simul_inputs.jpeg", sep = "/"), 
       width = 15, height = 12, units = "cm", dpi = 600
       )
