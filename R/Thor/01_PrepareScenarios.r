##--------------------------------------------------------------------------------------------------------
## SCRIPT : Prepare scenarios for RLA
##
## Authors : Matthieu Authier, Mathieu Genu and Phil Hammond
## Last update : 2021-07-10
##
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("RLA", "rstan", "tidyverse", "ggthemes", "MCMCpack"),
       library, character.only = TRUE
       )

rm(list = ls())

data("north_sea_hp")
set.seed(20210329)

n_sim <- 1e5
seeds <- sample.int(1e8, size = n_sim, replace = FALSE)
MNPL <- round(0.6 + 0.05 * rnorm(n_sim), 3)
cv_byc <- round(runif(n_sim, 0.05, 0.50), 3)
cv_env <- round(runif(n_sim, 0.00, 0.20), 3)
cv_obs <- round(runif(n_sim, 0.10, 0.40), 3)
MNP <- rep(1.04, n_sim)
r <- round(runif(n_sim, 0.001, 0.06), 3)

rla <- lapply(1:n_sim,
              function(i) {
                pellatomlinson_rla(MNPL = MNPL[i],
                                   K = north_sea_hp$life_history$K,
                                   L = north_sea_hp$life_history$L,
                                   eta = north_sea_hp$life_history$eta,
                                   phi = north_sea_hp$life_history$phi,
                                   m = north_sea_hp$life_history$maturity,
                                   MNP = MNP[i],
                                   # series of catches
                                   catches = bycatch_rw(n = 51,
                                                        K = north_sea_hp$life_history$K,
                                                        rate = r[i],
                                                        cv = cv_byc[i],
                                                        seed_id = seeds[i]
                                                        ),
                                   CV = cv_obs[i],
                                   CV_env = cv_env[i],
                                   # scans survey
                                   scans = c(29, 40, 51),
                                   verbose = FALSE,
                                   # for reproducibility purposes
                                   seed_id = seeds[i]
                                   )
              }
)

depletion <- data.frame(current_depletion = do.call('c',
                                                    lapply(rla,
                                                           function(l) {
                                                             l$depletion[length(l$depletion)]
                                                           })
                                                    )
                        ) %>%
  mutate(MNP = MNP,
         MNPL_cat = cut(MNPL, breaks = seq(0.2, 1.0, 0.05)),
         level = cut(current_depletion, breaks = c(seq(0.0, 0.9, 0.1), 1.0)),
         sim = 1:n() # indicator variable
         )

with(depletion, table(MNPL_cat, level))

### keep scenarios
set.seed(19790428)
n_sim <- c(1, 4, 27, 68, 68, 27, 4, 1)
MNPL_cat <- c("(0.4,0.45]", "(0.45,0.5]", "(0.5,0.55]", "(0.55,0.6]", "(0.6,0.65]", "(0.65,0.7]", "(0.7,0.75]", "(0.75,0.8]")
keep <- function(mnpl, depletion_level, n_sim) {
  dd <- depletion %>%
    filter(MNPL_cat == mnpl,
           level == depletion_level
           ) %>%
    slice_sample(n = n_sim) %>%
    mutate(MNPL = MNPL[sim],
           cv_byc = cv_byc[sim],
           cv_env = cv_env[sim],
           cv_obs = cv_obs[sim],
           rate = r[sim],
           seed = seeds[sim]
           ) %>% 
    dplyr::select(-sim) %>% 
    as.data.frame()
    return(dd)
}

param <- NULL
for(i in 1:length(MNPL_cat)) {
  for(j in c("(0.3,0.4]", "(0.4,0.5]", "(0.5,0.6]", "(0.6,0.7]", "(0.7,0.8]", "(0.8,0.9]")) {
    param <- rbind(param, keep(mnpl = MNPL_cat[i], depletion_level = j, n_sim = n_sim[i]))
  }
}

param %>% 
  head(5)
skimr::skim(param)

write.table(param, "20210710_HPNS_OMbase.txt", row.names = FALSE, col.names = TRUE, quote = FALSE, sep = "\t")

rla <- lapply(1:nrow(param),
              function(i) {
                pellatomlinson_rla(MNPL = param$MNPL[i],
                                   K = north_sea_hp$life_history$K,
                                   L = north_sea_hp$life_history$L,
                                   eta = north_sea_hp$life_history$eta,
                                   phi = north_sea_hp$life_history$phi,
                                   m = north_sea_hp$life_history$maturity,
                                   MNP = param$MNP[i],
                                   # series of catches
                                   catches = bycatch_rw(n = 51,
                                                        K = north_sea_hp$life_history$K,
                                                        rate = param$rate[i],
                                                        cv = param$cv_byc[i],
                                                        seed_id = param$seed[i]
                                                        ),
                                   CV = param$cv_obs[i],
                                   CV_env = param$cv_env[i],
                                   # scans survey
                                   scans = c(29, 40, 51),
                                   verbose = FALSE,
                                   # for reproducibility purposes
                                   seed_id = param$seed[i]
                                   )
              }
)

save(file = "20210710_HPNS_OMbase.RData", list = "rla")

### check
dd <- data.frame(current_depletion = do.call('c',
                                             lapply(rla,
                                                    function(l) {
                                                      l$depletion[length(l$depletion)]
                                                    })
                                             )
                        ) %>%
  mutate(MNP = param$MNP,
         MNPL_cat = cut(param$MNPL, breaks = seq(0.2, 1.0, 0.05)),
         level = cut(current_depletion, breaks = c(seq(0.0, 0.9, 0.1), 1.0)),
         sim = 1:n() # indicator variable
         )

with(dd, table(MNPL_cat, level))
pairs(param[, c(1, 5:9)])
