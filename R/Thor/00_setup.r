library(RLA)
library(rstan)

data(rlastan_models)
cat(rlastan_models$uniform,
    file = "uniform.stan"
    )

# compile model
rlastan <- rstan::stan_model(file = "uniform.stan",
                             model_name = "RLA"
                             )

save(file = "rlastan.RData", list = "rlastan")
